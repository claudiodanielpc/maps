#Análisis de datos de inmuebles y cuentas catastrales
#Fuente: https://sig.cdmx.gob.mx/datos/


# Librerías ====
if(!require('pacman')) install.packages('pacman')
pacman::p_load(tidyverse, leaflet,sf,htmltools)

# Parámetros previos ====
url<-"https://sig.cdmx.gob.mx/documents"

# User defined functions ====
descarga_y_unzip <- function(index_archivo) {
 temp <- tempfile()
  download.file(glue::glue("{url}/{index_archivo}/download"),
    mode = "wb",
    destfile = temp)
  unzip(temp, exdir = "catastro") 
  unlink(temp) 
}

#Establecer directorio de trabajo
setwd("C:/Users/claud/Documents/")

#Descarga los archivos 77 y 99
descarga_y_unzip(77)
descarga_y_unzip(99)

#Leer el csv
catastrodata<-read.csv("catastro/sig_cdmx_BENITO JUAREZ_08-2020.csv", header = TRUE, sep = ",", dec = ".", stringsAsFactors = FALSE)
nrow(catastrodata)


#Leer el shape
catastro<-st_read("catastro/BENITO_JUAREZ.shp")%>%
#Pegar el csv
  right_join(catastrodata)%>%

#Hacer las categorías de clave_rango_nivel
mutate(clave_rango_cat=case_when(
  clave_rango_nivel=="01" ~ "Superficies construidas descubiertas",
  clave_rango_nivel=="02" ~ "De 1 a 2 niveles o altura de hasta 6 metros",
  clave_rango_nivel=="05" ~ "De 3 a 5 niveles o altura de hasta 15 metros",
  clave_rango_nivel=="10" ~ "De 6 a 10 niveles",
  clave_rango_nivel=="15" ~ "De 11 a 15 niveles",
  clave_rango_nivel=="20" ~ "De 16 a 20 niveles",
  clave_rango_nivel=="99" ~ "De 21 o más niveles",
  clave_rango_nivel=="RU" ~ "Rango único",
  TRUE ~ "No disponible"
))
nrow(catastro)



  #Leer archivo de desarrollos inmobiliarios irregulares
urlirreg<-"https://datos.cdmx.gob.mx/dataset/0e60be38-58b4-4f7e-b9f5-0c326d196c75/resource/1fac6305-35e3-43c5-b3ba-db814ec6cc8c/download/desarrollos_irregulares.csv"
irregulares<-read.csv(urlirreg, header = TRUE, sep = ",", dec = ".", stringsAsFactors = FALSE,
encoding = "UTF-8")%>%
  #Transformar a sf
  st_as_sf(coords = c("longitud", "latitud"), crs = 4326)


#Crear una paleta de colores para el mapa
pal <- colorFactor(palette = "Paired", domain = catastro$clave_rango_cat)

#Crear mapa de leaflet con el catastro
leaflet(catastro)%>%
  addTiles() %>%
  #addProviderTiles("CartoDB.Positron")%>%
  #Añadir poligonos  y pintar de acuerdo a la paleta de colores
  addPolygons(
    group = "Cuentas catastrales",
    fillColor = ~pal(clave_rango_cat), fillOpacity = 0.5, color = "#bdbdbd", 
    weight = 1, popup = paste(
"<b>","Calle: ","</b>",catastro$call_numero,"<br>",
"<b>","Rango de niveles: ","</b>",catastro$clave_rango_cat,"<br>"
  ))%>%

  addCircleMarkers(data = irregulares, 
  group = "Desarrollos irregulares",
  radius = 5, color = "blue", fillOpacity = 0.5, popup = paste(
"<b>","Calle: ","</b>",irregulares$ubicacion,"<br>",
"<b>","Niveles construidos: ","</b>",irregulares$niveles_cons,"<br>",
"<b>","Niveles permitidos: ","</b>",irregulares$niveles_perm,"<br>"
  ))%>%
  #Añadir leyenda
  addLegend(
    pal = pal, 
  values = ~clave_rango_cat,
  opacity = 0.5, position = "topright",
  title="Categorías de niveles",
  )%>%
  #Control de capas
  addLayersControl(
   overlayGroups = c("Cuentas catastrales", "Desarrollos irregulares"), 
   options = layersControlOptions(collapsed = FALSE))%>%
   #Desactivar
  hideGroup(c("Cuentas catastrales"))->mapa



htmltools::save_html(mapa, "mapa_irregulares.html")


